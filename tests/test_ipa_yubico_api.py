#!/usr/bin/python3
"""Simple test for APIs used by python-yubico
"""
import logging

import usb.core
import usb.legacy

logging.basicConfig(level=logging.INFO)
log = logging.getLogger()


def main():
    for d in usb.core.find(find_all=True):
        usb_device = usb.legacy.Device(d)
        # import pdb; pdb.set_trace()
        log.info(vars(usb_device))
        assert usb_device.idVendor
        assert usb_device.idProduct
        usb_conf = usb_device.configurations[0]
        log.info(vars(usb_conf))
        usb_int = usb_conf.interfaces[0][0]
        try:
            usb_handle = usb_device.open()
            assert usb_handle.controlMsg.__call__
            usb_handle.releaseInterface()
        except usb.core.USBError:
            log.info("Unable to open USB device")
    log.info("PASS")


if __name__ == "__main__":
    main()
